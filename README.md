# Raspberry Pi Pico W Blinky

## If This Helps You, Please Star The Original Component Source Project
One click can help us keep providing and improving this component. If you find this information helpful, please click the star on this components original source here: [Project Details](https://gitlab.com/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk)

This project completely automates Sections 1 through 3.1 of https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf

You can download the built firmware from [this projects releases](https://gitlab.com/guided-explorations/embedded/ci-components/working-code-examples/raspberry-pi-pico-w-blinky/-/releases) to flash a Pico W and test it.